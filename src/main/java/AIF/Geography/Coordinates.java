package AIF.Geography;

public class Coordinates {
    private final Double longitude;
    private final Double latitude;

    public Coordinates(Double longitude, Double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Double distance(Coordinates other) {
        double result = Math.pow(this.latitude - other.getLatitude(), 2) + Math.pow(this.longitude - other.getLongitude(), 2);
        return Math.sqrt(result);
    }
}
