package AIF.AerialVehicles.Entities;


import AIF.AerialVehicles.Equipment.Module;
import AIF.AerialVehicles.Exceptions.*;
import AIF.Geography.Coordinates;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class AerialVehicle {
    private boolean isInAir = false;
    private boolean isInGround = true;
    private final Coordinates currentCoordinates;
    private double range;
    private final double defaultRange;
    private final Map<String, Boolean> stationsAvailable;
    private int totalStations;
    private Map<String, Integer> numberOfStations;
    private boolean isWeaponLoaded = false;
    private boolean isWeaponUsed = false;

    public AerialVehicle(final Coordinates coordinates, final double range, final int totalStations, final Map<String, Boolean> equipmentAvailable) {
        this.currentCoordinates = coordinates;
        this.range = range;
        this.defaultRange = range;
        this.stationsAvailable = equipmentAvailable;
        this.totalStations = totalStations;
        this.numberOfStations = calculateNumberOfStations(equipmentAvailable);
    }

    public void takeOff() throws CannotPerformInMidAirException {
        if (this.isInGround) {
            System.out.println("Taking off...");
            isInAir = true;
            isInGround = false;
        } else {
            throw new CannotPerformInMidAirException();
        }
    }

    public void flyTo(final Coordinates coordinates) throws CannotPerformOnGroundException, NeedMaintenanceException {
        if (!needMaintenance(coordinates)) {
            if (this.isInAir) {
                System.out.println("Flying to: " + coordinates);
                range -= this.currentCoordinates.distance(coordinates);
            } else {
                throw new CannotPerformOnGroundException();
            }
        } else {
            throw new NeedMaintenanceException();
        }
    }

    public void land() throws CannotPerformOnGroundException {
        if (this.isInAir) {
            System.out.println("Landing...");
            this.isInGround = true;
            this.isInAir = false;
        } else {
            throw new CannotPerformOnGroundException();
        }
    }

    public Boolean needMaintenance(Coordinates lastMaintenanceLocation) {
        return currentCoordinates.distance(lastMaintenanceLocation) > this.range;
    }

    public void performMaintenance() throws CannotPerformInMidAirException {
        if (this.isInGround) {
            System.out.println("Performing maintenance");
            resetDistanceRange();
        } else {
            throw new CannotPerformInMidAirException();
        }
    }

    private void resetDistanceRange() {
        this.range = this.defaultRange;
    }

    protected Boolean isInAir() {
        return this.isInAir;
    }

    public void loadModule(final Module module) throws InterruptedException, NoModuleStationAvailableException, ModuleNotCompatibleException, ModuleNotFoundException, NoModuleCanPerformException {
        final String moduleName = module.getClass().getSimpleName();

        try {
            checkIfModuleExists(moduleName);
        } catch (final ModuleNotFoundException e) {
            System.out.println(e.getMessage() + ", try again :)");
            return;
        }

        if (moduleName.equals("WeaponModule")) {
            isWeaponUsed();
        }
        if (this.numberOfStations.get(moduleName) == 0) {
            throw new NoModuleStationAvailableException();
        } else if (!this.stationsAvailable.get(moduleName)) {
            throw new ModuleNotCompatibleException();
        } else {
            System.out.println("Loading the module of: " + moduleName);
            TimeUnit.SECONDS.sleep(3);
            System.out.println("Successfully loaded the module: " + moduleName);
            this.isWeaponLoaded = true;
        }
    }

    public void activateModule(final AerialVehicle vehicleName, final Module module) throws Exception {
        final String moduleName = module.getClass().getSimpleName();
        if (this.isWeaponLoaded) {
            System.out.println("Activating the: " + moduleName + "module for the " + vehicleName.getClass().getSimpleName() + "vehicle");
            module.activate();
            reduceNumberOfStationsAvailable(moduleName);
        } else {
            throw new Exception("Weapon is not loaded, please load your equipment before trying to activate it");
        }
    }

    private void reduceNumberOfStationsAvailable(final String moduleName) {
        this.numberOfStations.replace(moduleName, this.numberOfStations.get(moduleName), this.numberOfStations.get(moduleName) - 1);
    }

    private Map<String, Integer> calculateNumberOfStations(final Map<String, Boolean> equipmentAvailable) {
        Random random = new Random();
        Map<String, Integer> numberOfStations = new HashMap<>();
        AtomicInteger generatedNumber = new AtomicInteger();
        equipmentAvailable.forEach((module, bool) -> {
            if (totalStations < 1) {
                numberOfStations.put(module, 0);
            } else if (bool) {
                generatedNumber.set(random.nextInt(totalStations));
                numberOfStations.put(module, generatedNumber.get());
            }
            this.totalStations -= generatedNumber.get();
        });
        return numberOfStations;
    }

    private void checkIfModuleExists(final String moduleName) throws ModuleNotFoundException {
        if (this.numberOfStations.get(moduleName) == null) {
            throw new ModuleNotFoundException();
        }
    }

    private void isWeaponUsed() throws NoModuleCanPerformException {
        if (this.isWeaponUsed) {
            throw new NoModuleCanPerformException();
        } else {
            this.isWeaponUsed = true;
        }
    }
}