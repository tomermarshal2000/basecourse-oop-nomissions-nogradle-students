package AIF.AerialVehicles.Entities.FighterJets;


import AIF.Geography.Coordinates;

import java.util.Map;

public class F15 extends FighterJet {
    private static final Map<String, Boolean> EQUIPMENT_AVAILABLE = Map.of("BDAModule", false, "IntelligenceModule", true, "WeaponModule", true);
    private static final int TOTAL_STATIONS = 10;

    public F15(Coordinates coordinates, double range) {
        super(coordinates, range, TOTAL_STATIONS, EQUIPMENT_AVAILABLE);
    }
}