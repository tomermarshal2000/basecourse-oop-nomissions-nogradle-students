package AIF.AerialVehicles.Entities.FighterJets;

import AIF.AerialVehicles.Entities.AerialVehicle;
import AIF.Geography.Coordinates;

import java.util.Map;

public class FighterJet extends AerialVehicle {
    public FighterJet(final Coordinates coordinates, final double range, final int totalStations, final Map<String, Boolean> stationsAvailable) {
        super(coordinates, range, totalStations, stationsAvailable);
    }
}
