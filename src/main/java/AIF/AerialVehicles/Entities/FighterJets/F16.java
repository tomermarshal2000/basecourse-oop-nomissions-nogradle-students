package AIF.AerialVehicles.Entities.FighterJets;

import AIF.Geography.Coordinates;

import java.util.Map;

public class F16 extends FighterJet {
    private static final Map<String, Boolean> EQUIPMENT_AVAILABLE = Map.of("BDAModule", true, "IntelligenceModule", false, "WeaponModule", false);
    private static final int TOTAL_STATIONS = 7;

    public F16(Coordinates coordinates, double range) {
        super(coordinates, range, TOTAL_STATIONS, EQUIPMENT_AVAILABLE);
    }
}