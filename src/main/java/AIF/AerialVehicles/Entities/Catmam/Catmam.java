package AIF.AerialVehicles.Entities.Catmam;

import AIF.AerialVehicles.Entities.AerialVehicle;
import AIF.AerialVehicles.Exceptions.CannotPerformOnGroundException;
import AIF.Geography.Coordinates;

import java.util.Map;

public class Catmam extends AerialVehicle {
    private final Coordinates coordinates;

    public Catmam(final Coordinates coordinates, final double range, final int totalStations, final Map<String, Boolean> equipmentAvailable) {
        super(coordinates, range, totalStations, equipmentAvailable);
        this.coordinates = coordinates;
    }

    void hoverOverLocation(final int hours) throws CannotPerformOnGroundException {
        if (isInAir()) {
            System.out.printf("Hovering over: [%,.2f][%,.2f]%n", coordinates.getLatitude(), coordinates.getLongitude());
        } else {
            throw new CannotPerformOnGroundException();
        }
    }
}
