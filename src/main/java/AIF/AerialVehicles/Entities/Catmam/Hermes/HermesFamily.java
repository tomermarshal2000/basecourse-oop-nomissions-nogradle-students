package AIF.AerialVehicles.Entities.Catmam.Hermes;

import AIF.AerialVehicles.Entities.Catmam.Catmam;
import AIF.Geography.Coordinates;

import java.util.Map;

public class HermesFamily extends Catmam {
    public HermesFamily(final Coordinates coordinates, final int totalStations, final Map<String, Boolean> equipmentAvailable) {
        super(coordinates, 10000, totalStations, equipmentAvailable);
    }
}
