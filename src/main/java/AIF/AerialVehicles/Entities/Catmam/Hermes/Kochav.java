package AIF.AerialVehicles.Entities.Catmam.Hermes;

import AIF.Geography.Coordinates;

import java.util.Map;

public class Kochav extends HermesFamily {
    private static final Map<String, Boolean> EQUIPMENT_AVAILABLE = Map.of("BDAModule", true, "IntelligenceModule", true, "WeaponModule", true);
    private static final int TOTAL_STATIONS = 5;

    public Kochav(Coordinates coordinates) {
        super(coordinates, TOTAL_STATIONS, EQUIPMENT_AVAILABLE);
    }
}