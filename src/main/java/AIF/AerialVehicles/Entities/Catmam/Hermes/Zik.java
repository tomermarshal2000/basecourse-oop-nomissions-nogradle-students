package AIF.AerialVehicles.Entities.Catmam.Hermes;


import AIF.Geography.Coordinates;

import java.util.Map;

public class Zik extends HermesFamily {
    private static final Map<String, Boolean> EQUIPMENT_AVAILABLE = Map.of("BDAModule", true, "IntelligenceModule", true, "WeaponModule", false);
    private static final int TOTAL_STATIONS = 1;

    public Zik(Coordinates coordinates) {
        super(coordinates, TOTAL_STATIONS, EQUIPMENT_AVAILABLE);
    }
}