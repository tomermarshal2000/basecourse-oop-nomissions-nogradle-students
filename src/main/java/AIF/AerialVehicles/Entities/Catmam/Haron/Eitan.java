package AIF.AerialVehicles.Entities.Catmam.Haron;

import AIF.Geography.Coordinates;

import java.util.Map;

public class Eitan extends HaronFamily {
    private static final Map<String, Boolean> EQUIPMENT_AVAILABLE = Map.of("BDAModule", false, "IntelligenceModule", true, "WeaponModule", true);
    private static final int TOTAL_STATIONS = 4;

    public Eitan(Coordinates coordinates) {
        super(coordinates, TOTAL_STATIONS, EQUIPMENT_AVAILABLE);
    }
}