package AIF.AerialVehicles.Entities.Catmam.Haron;

import AIF.AerialVehicles.Entities.Catmam.Catmam;
import AIF.Geography.Coordinates;

import java.util.Map;

public class HaronFamily extends Catmam {
    public HaronFamily(final Coordinates coordinates, final int totalStations, final Map<String, Boolean> equipmentAvailable) {
        super(coordinates, 15000, totalStations, equipmentAvailable);
    }
}
