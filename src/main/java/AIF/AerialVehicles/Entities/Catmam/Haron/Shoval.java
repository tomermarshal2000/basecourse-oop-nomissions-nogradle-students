package AIF.AerialVehicles.Entities.Catmam.Haron;


import AIF.Geography.Coordinates;

import java.util.Map;

public class Shoval extends HaronFamily {
    private static final Map<String, Boolean> EQUIPMENT_AVAILABLE = Map.of("BDAModule", true, "IntelligenceModule", true, "WeaponModule", true);
    private static final int TOTAL_STATIONS = 3;

    public Shoval(Coordinates coordinates) {
        super(coordinates, TOTAL_STATIONS, EQUIPMENT_AVAILABLE);
    }
}