package AIF.AerialVehicles.Exceptions;

public class NoModuleCanPerformException extends AVException {
    public NoModuleCanPerformException() {
        super("No loaded module of the relevant type currently able to perform the task.");
    }
}
